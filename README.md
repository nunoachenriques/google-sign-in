[![Android 6.0+](https://img.shields.io/badge/mobile-Android%206.0%2B-%2378C257.svg "Android 6.0+")][1]
[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][2]

# Google sign-in for Android

## Case study

A Google sign in demo adapted from
<https://developers.google.com/identity/sign-in/android/start-integrating>
to test authentication with OAuth2 and getting ID token using JSON secrets
file in `assets`.

An additional class is used to test sending ID token to a custom Web service.

The data flow architecture is depicted below: 

![Data flow](README-arch.png "Data flow architecture")

## Development

### Android Studio (recommended)

 1. Fork or download project source code.
 2. Use Android Studio to open the project.
 3. Use emulators or connected devices and the usual actions.

### How to

 1. Create or select a project at
    <https://console.developers.google.com/apis/credentials>  (top bar, pull
    down menu).

 2. Create *OAuth client ID* credentials:
 
    1. Use the `~/.android/debug.keystore` (password: *android*) to get SHA-1
       certificate fingerprint. This is the default signing certificate for
       running the app as a `debug` build (check `app/build.gradle`
       » `android` » `buildTypes` where two types are present: `debug` and
       `release`):
    
       `keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore -list -v`
       
       More: <https://developers.google.com/android/guides/client-auth>
       
    2. Package name from `AndroidManifest.xml` » `manifest` » `package`
    
 3. Use *OAuth client ID* credentials:
  
    1. Download JSON from <https://console.developers.google.com/apis/credentials>
       to `assets/google_sign_in.json.secret` (**already in `.gitignore`**).
  
    2. (**OPTIONAL**) To test the send ID token then create a JSON file
       `assets/send_id_token.json.secret` (**already in `.gitignore`**):
       
       `{"url": "https://your_service_backend/"}`
       
       If running on Android emulator and using a `localhost` server in your
       desktop machine then use IP address `10.0.2.2` as `your_service_backend`.
       
       Furthermore, change `xml/network_security_config.xml` to reflect your
       case. It is prepared for a local Web server and Android emulator where
       a certificate must be created for the Web server and then copied to
       `raw/self_signed_crt_pem.secret`. Remember when creating the certificate
       to add `IP.1 = 10.0.2.2` to the `subjectAltName`.
    
    3. Do your thing... A full example is already coded:
       `requestIdToken(clientId)`. Hack, modify, test, repeat.
    
 4. (**OPTIONAL**) If building as a `release` type then a new *OAuth client ID*
    must be created with the SHA-1 fingerprint from your app signing
    certificate (usually another keystore created by you).
    Also, adapt to the released package name if changed (`AndroidManifest.xml`). 

## License

Copyright 2019 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Trademarks

Android is a trademark of Google LLC.

[1]: https://developer.android.com/about/versions/marshmallow
[2]: http://www.apache.org/licenses/LICENSE-2.0.html
