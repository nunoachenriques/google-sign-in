/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.googlesignin;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Useful to deal with files and JSON data in assets folder.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class Assets {

    private static final String TAG = "Assets";

    private Assets() {}

    /**
     * Get JSON format data stored in a file.
     *
     * @param ctx Android application context.
     * @param filename The name of the file.
     * @return File contents in a string.
     */
    static String readJSONDataFromAsset(Context ctx, String filename) {
        try (InputStream is = ctx.getAssets().open(filename)) {
            int size = is.available();
            byte[] buffer = new byte[size];
            if (is.read(buffer) != size) {
                throw new IOException("read size DIFFERS from available!");
            }
            return new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            Log.e(TAG, filename + " open FAILED!");
            return "";
        }
    }
}
