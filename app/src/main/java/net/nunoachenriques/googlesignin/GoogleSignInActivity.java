/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.googlesignin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.auth.api.signin.*;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * User interface activity to demonstrate the use of Google OAuth2 identity
 * service to log in a user. Additionally, stores the ID token in Android
 * SharedPreferences for sharing with another class and use it, for instance,
 * as data identification for remote storage.
 * <br /><br />
 * Adapted from:
 *  <pre>
 *  https://developers.google.com/identity/smartlock-passwords/android/overview
 *  https://developers.google.com/identity/sign-in/android/start-integrating
 *  </pre>
 *
 * NOTICE:
 *  <pre>
 *  https://console.developers.google.com/apis/credentials
 *
 *  Use the ~/.android/debug.keystore (password: android) to get SHA-1
 *  certificate fingerprint when debugging app (testing in emulator):
 *
 *  keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore -list -v
 *  </pre>
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class GoogleSignInActivity
        extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "GoogleSignInActivity";
    private static final String ID_TOKEN_NAME = "id_token";
    private static final int HUMAN_SIGN_IN_RC = 33;

    private SharedPreferences sharedPreferences;
    private GoogleSignInClient googleSignInClient;
    private TextView profileTextView;
    private SignInButton signInButton;
    private Button signOutButton;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        setContentView(R.layout.google_sign_in);
        googleSignInClient = getGoogleSignInClient();
        if (googleSignInClient == null) {
            throw new RuntimeException("googleSignInClient is NULL! Hint: check assets files.");
        }
        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(this);
        signOutButton = findViewById(R.id.sign_out_button);
        signOutButton.setOnClickListener(this);
        sendButton = findViewById(R.id.send_button);
        profileTextView = findViewById(R.id.profile);
    }

    @Override
    protected void onStart() {
        super.onStart();
        signInAutoRefresh();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
            case R.id.send_button:
                sendIdToken();
                break;
        }
    }

    private void signIn() {
        Log.i(TAG, "* * * signIn");
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, HUMAN_SIGN_IN_RC);
    }

    private void signOut() {
        Log.i(TAG, "* * * signOut");
        googleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUIAccountAndToken(null);
                    }
                });
    }

    /*
     * 1. Update if already sign in, refresh token if expired.
     * 2. Ask for sign in if first time, signed out or revoked.
     *
     * https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInClient#silentSignIn()
     */
    private void signInAutoRefresh() {
        Log.i(TAG, "* * * signInAutoRefresh");
        Task<GoogleSignInAccount> task = googleSignInClient.silentSignIn();
        if (task.isSuccessful()) {
            GoogleSignInAccount account = task.getResult();
            updateUIAccountAndToken(account);
        } else {
            task.addOnCompleteListener(new OnCompleteListener<GoogleSignInAccount>() {
                @Override
                public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                    try {
                        GoogleSignInAccount account = task.getResult(ApiException.class);
                        Log.i(TAG, "signInAutoRefresh account: ["
                                + (account != null ? account.toString() : null) + "]");
                        updateUIAccountAndToken(account);
                    } catch (ApiException e) {
                        final int status = e.getStatusCode();
                        Log.w(TAG, "signInAutoRefresh FAILED: "
                                + "[" + status + "] "
                                + GoogleSignInStatusCodes.getStatusCodeString(status));
                        if (status == GoogleSignInStatusCodes.SIGN_IN_REQUIRED) {
                            signIn();
                        } else {
                            updateUIAccountAndToken(null);
                        }
                    }
                }
            });
        }
    }

    private void sendIdToken() {
        signInAutoRefresh();
        String idToken = sharedPreferences.getString(ID_TOKEN_NAME, null);
        if (idToken != null) {
            SendIdTokenService.startActionSend(this, idToken);
            Log.i(TAG, "ID token SENT.");
        } else {
            Log.w(TAG, "ID token NULL!");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HUMAN_SIGN_IN_RC) {
            // The Task returned from this call is always completed,
            // no need to attach a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> doneTask) {
        try {
            GoogleSignInAccount account = doneTask.getResult(ApiException.class);
            Log.i(TAG, "handleSignInResult account: ["
                    + (account != null ? account.toString() : null) + "]");
            updateUIAccountAndToken(account);
        } catch (ApiException e) {
            final int status = e.getStatusCode();
            Log.w(TAG, "handleSignInResult FAILED: "
                    + "[" + status + "] "
                    + GoogleSignInStatusCodes.getStatusCodeString(status));
            updateUIAccountAndToken(null);
        }
    }

    private void updateUIAccountAndToken(GoogleSignInAccount account) {
        final StringBuilder sb = new StringBuilder("NOT SIGNED IN YET!\n");
        String idToken = null;
        if (account != null) {
            idToken = account.getIdToken();
            sb.delete(0, sb.length())
                    .append(account.getDisplayName()).append("\n")
                    .append(account.getGivenName()).append("\n")
                    .append(account.getFamilyName()).append("\n")
                    .append(account.getEmail()).append("\n")
                    .append(account.getId()).append("\n")
                    .append(account.getPhotoUrl()).append("\n")
                    .append(idToken);
            signInButton.setEnabled(false);
            signOutButton.setEnabled(true);
        } else {
            signInButton.setEnabled(true);
            signOutButton.setEnabled(false);
        }
        profileTextView.setText(sb.toString());
        // Update shared storage and UI button send
        if (idToken != null) {
            sharedPreferences
                    .edit()
                    .putString(ID_TOKEN_NAME, idToken)
                    .apply();
            sendButton.setOnClickListener(this);
            sendButton.setEnabled(true);
        } else {
            sendButton.setEnabled(false);
        }
        Log.i(TAG, sb.toString());
    }

    private GoogleSignInClient getGoogleSignInClient() {
        try {
            final JSONObject sec = new JSONObject(
                    Assets.readJSONDataFromAsset(this, "google_sign_in.json.secret"));
            final GoogleSignInOptions gso = new GoogleSignInOptions
                    .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestIdToken(sec.getJSONObject("web").getString("client_id"))
                    .build();
            return GoogleSignIn.getClient(this, gso);
        } catch (JSONException e) {
            Log.e(TAG, "JSON object access FAILED!");
            return null;
        }
    }
}
