/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.googlesignin;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread. Case study: send ID token to a
 * remote service.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public class SendIdTokenService
        extends IntentService {

    private static final String TAG = "SendIdTokenService";
    private static final String ACTION_SEND = "net.nunoachenriques.googlesignin.action.SEND";
    private static final String EXTRA_ID_TOKEN = "net.nunoachenriques.googlesignin.extra.EXTRA_ID_TOKEN";

    public SendIdTokenService() {
        super("SendIdTokenService");
    }

    /**
     * Starts this service to perform action with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @param c Android application context.
     * @param idToken The content to send: an ID token string.
     * @see IntentService
     */
    public static void startActionSend(Context c, String idToken) {
        Intent intent = new Intent(c, SendIdTokenService.class);
        intent.setAction(ACTION_SEND);
        intent.putExtra(EXTRA_ID_TOKEN, idToken);
        c.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent i) {
        if (i != null) {
            final String action = i.getAction();
            if (ACTION_SEND.equals(action)) {
                final String idToken = i.getStringExtra(EXTRA_ID_TOKEN);
                handleActionSend(idToken);
            } else {
                Log.e(TAG, "Action [" + action + "] UNKNOWN!");
            }
        }
    }

    private void handleActionSend(String idToken) {
        try {
            Log.d(TAG, "getURL: " + getURL());
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST,
                    getURL(),
                    new JSONObject("{\"id_token\":\"" + idToken + "\"}"),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(SendIdTokenService.this,
                                    "Server JSON response: " + response.toString(), Toast.LENGTH_LONG).show();
                            Log.i(TAG, response.toString());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // VolleyError error is null (already consumed)
                        }
                    });
            requestQueue.add(request);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private String getURL() {
        try {
            JSONObject jsonObject = new JSONObject(Assets
                    .readJSONDataFromAsset(this, "send_id_token.json.secret"));
            return jsonObject.getString("url");
        } catch (JSONException e) {
            Log.e(TAG, "JSON object access FAILED!");
            return "";
        }
    }
}
